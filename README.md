# Webapp Angular

Website template for an Angular only website
With many homemade component for customizing your website

## What is working
- [x] Install and launch website
- [x] Can be deploy in Docker 

## Instalation

```
git clone https://gitlab.com/Roofne/project-template-website-angular
ng serve --open
```