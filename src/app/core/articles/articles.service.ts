import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'

@Injectable({
  providedIn: 'root'
})
export class ArticlesService {

  constructor(private http:HttpClient) { }

  getData() {
    let url="http://localhost:8080/projet/webapi/articles/";
    return this.http.get(url);
  }
}


