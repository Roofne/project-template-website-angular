import { Component } from '@angular/core';
import { ArticlesService } from './core/articles/articles.service'; 

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'src'
  data  = [] as any

  constructor(private articles:ArticlesService) {
    this.articles.getData().subscribe(data => {
      console.warn(data)
      this.data=data
    })
  }
}
